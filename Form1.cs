﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace Test__Start_WinApp
{
     public partial class Form1 : Form
    {
        static System.Timers.Timer timer;
        
        public Form1()
        {
            SetTimerJob();
            InitializeComponent();

        }

        private static void SetTimerJob()
        {
            timer = new System.Timers.Timer();
            timer.Interval = GetTimeUntilNextMinute(1);

            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            timer.Enabled = true;
        }

        private void EmailWait()
        {
            EventWaitHandle handle =
           new EventWaitHandle(false, EventResetMode.ManualReset, "GoodMutexName");

            while (true)
            {
                handle.WaitOne();

                WriteToFile();

                handle.Reset();
            }
        }

        static  void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            WriteToFile();
         
        }

        static double GetInterval()
        {
            DateTime now = DateTime.Now;
            return ((60 - now.Second) * 1000 - now.Millisecond);
        }

        private  static void WriteToFile()
        {
            string data = "\n Initail Write file with time - " + DateTime.Now.ToLongDateString();
            string path = "D:\\Manju\\ServiceLog.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(data, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
            // Add timer code here
        }
       
        private static double CalculateInterval()
        {
           
            // 1 AM the next day
            return  (DateTime.Now.AddDays(1).Date.AddMinutes(1) - DateTime.Now).TotalMilliseconds;
        }
        /// <summary>
        /// Returns the period of time left before the specified hour is due to elapse
        /// </summary>
        /// <param name="hour">And integer representing an hour, 
        /// where 0 is midnight, 12 is midday, 23 is eleven et cetera</param>
        /// <returns>A TimeSpan representing the calculated time period</returns>
        public static double GetTimeUntilNextMinute(int min)
        {
            var currentTime = DateTime.Now;
            var desiredTime = new DateTime(DateTime.Now.Year,
                DateTime.Now.Month, DateTime.Now.Day, 0, min, 0);
            var timeDifference = (currentTime - desiredTime);
            var timePeriod = currentTime.Minute >= min ?
                (desiredTime.AddDays(1) - currentTime) :
                -timeDifference;
            return timePeriod.TotalMilliseconds;
        }
    }
}
